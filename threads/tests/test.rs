use std::sync::{Arc,Mutex};

use threads::players::players::Player;
use threads::players::players::PlayerType;

#[test]
/// Test sulla generazione di un numero random da parte del client.
pub fn test_generate_number(){
    let mut player = Player{id_client: 1,p_type:PlayerType::Smart,send_number: Some(10),ordine: None,max: 100,min:1};
    assert_eq!(55, player.generate_random_number("<"));
}

#[test]
/// Test sul controllo da parte del client della sua vittoria o meno.
pub fn test_check_win(){
    let counter = Arc::new(Mutex::new(0));
    let mut player = Player{id_client: 1,p_type:PlayerType::Smart,send_number: Some(52),ordine: None,max: 100,min:1};
    assert_eq!(true,player.check_win(counter));
}