//! Modulo Players.
//! 
//! Fornisce le strutture, metodi e funzioni per la gestione delle attività dei client.
//! 
//! Sono definiti tre tipi di giocatori:\
//!     - giocatori Simple1: provano un primo numero random e in base al responso del server provano i numeri successivi in modo crescente o decrescente,\
//!     - giocatori Simple2: provano numeri random senza ricordarsi il numero appena inviato,\
//!     - giocatori Smart: provano i numeri utilizzando la ricerca binaria.\
/// Modulo dei giocatori.
pub mod players{

    use std::{sync::mpsc::Sender};
    use std::sync::{mpsc, Arc, Mutex};
    use std::{thread};
    use rand::Rng;
    use std::time::Duration;
    
    /// Struttura che contiene il messaggio inviato dai client.
    pub struct MsgClient{
        /// Id del client che ha inviato il messaggio
        pub id_client: i32,
        /// Messaggio inviato dal client
        pub msg: i32,
        /// E' l'endpoint Sender inviato dal client e che il server deve utilizzare per dare un feedback (<,>,=) relativo al numero inviato dal client.
        pub sender : Sender<String>
    }
    /// Enumeratore che definisce i possibili tipi di un giocatore.
    #[derive(Debug)]
    pub enum PlayerType{
        Simple1,
        Simple2,
        Smart
    }
    #[derive(PartialEq)]
    /// Enumeratore che definisce l'ordine dei numeri da inviare dopo il primo (solo per giocatori Smart1).
    pub enum OrderNumber{
        Growing,
        Descending,
    }
    /// Struttura che contiene un giocatore.
    pub struct Player{
        /// Id del client
        pub id_client : i32,
        /// Tipo del client (Simple1, Simple2, Smart)
        pub p_type: PlayerType,
        /// Numero inviato (solo per Simple1 e Smart)
        pub send_number: Option<i32>,
        /// Ordine con cui inviare i numeri successivi al primo (Some per Simple1, None per gli altri tipi)
        pub ordine: Option<OrderNumber>,
        /// Valore massimo del range
        pub max:i32,
        /// Valore minimo del range
        pub min:i32
    }

    impl Player{

        /// Metodo che effettua la ricerca binaria per definire il nuovo numero da inviare.\
        /// Richiamato solo per giocatori di tipo Smart.\
        /// Ritorna un i32 che è il numero generato da inviare.
        /// 
        /// # Argomenti
        /// 
        /// * 'self' - Una struttura Player
        /// * 'responso' - Il feedback mandato dal server relativamente all'ultimo numero inviato (>, <, =)
        /// 
        fn binary_search(&mut self, responso:&str) -> i32 {

            if responso == String::from(">") {
                    self.max = self.send_number.unwrap() - 1;
                }
            else if responso == String::from("<"){
                    self.min = self.send_number.unwrap() + 1;
            }
            let numero = (self.min+self.max)/2;
            
            numero
            
        }

        /// Metodo che genera il nuovo numero da inviare.\
        /// Ritorna un i32 che è il numero da inviare.
        /// 
        /// # Argomenti
        /// 
        /// * 'self' - Una struttura Player
        /// * 'responso' - Il feedback mandato dal server relativamente all'ultimo numero inviato (>, <, =)
        /// 
        /// # Esempi
        /// ```
        /// use threads::players::players::Player;
        /// use threads::players::players::PlayerType;
        /// 
        /// let mut player = Player{id_client: 1,p_type:PlayerType::Smart,send_number: Some(10),ordine: None,max: 100,min:1};
        /// let num = player.generate_random_number("<");
        /// ```
        pub fn generate_random_number(&mut self, responso:&str)-> i32{

            let mut rng = rand::thread_rng();
            let mut number = rng.gen_range(self.min..self.max);

            match self.p_type{
                PlayerType::Simple1=>{
                    // Se il client non è al primo numero da inviare
                    if self.send_number.unwrap() != 0{
                        
                        match &self.ordine{
                            // Se è già stato definito un ordinamento
                            Some(order) =>{
                                if *order ==  OrderNumber::Growing{
                                    self.send_number = Some(self.send_number.unwrap()+1);
                                    number = self.send_number.unwrap();
                                }
                                else{
                                    self.send_number= Some(self.send_number.unwrap()-1);
                                    number = self.send_number.unwrap();
                                }
                                
                            }
                            // Se non è ancora stato definito un ordinamento
                            None=>{
                                if responso == "<"{
                                    self.ordine = Some(OrderNumber::Growing);
                                    self.send_number = Some(self.send_number.unwrap()+1);
                                    number = self.send_number.unwrap();
                                }
                                else{ 
                                    // Entra solo se il responso del server è > in quanto se il responso fosse = 
                                    // allora la funzione generate_random_number non è richiamata
                                    self.ordine = Some(OrderNumber::Descending);
                                    self.send_number= Some(self.send_number.unwrap()-1);
                                    number = self.send_number.unwrap();
                                }
                            }
                        }
                    }
                    else{
                        self.send_number = Some(number);
                    }
                    
                    number
                },
                PlayerType::Simple2=>{
                    number
                },
                PlayerType::Smart=>{
                    
                    self.send_number= Some(self.binary_search(responso));
                    number = self.send_number.unwrap();
                    number
                },
                
            }
        }

        /// Metodo che gestisce l'invio del messaggio del client al server e la ricezione del feedback del 
        /// server da parte del client.\
        /// 
        /// # Argomenti
        /// 
        /// * 'player' - Una struttura Player
        /// * 'tx' - L'endpoint Sender di un channel
        /// 
        pub fn client(self:&mut Player,tx: Sender<MsgClient>){
            let mut responso = String::from("");

            while responso != "="{

                thread::sleep(Duration::from_secs(1));

                //Il client genera un numero random in base al suo tipo e al responso ricevuto dal server.
                let generate_num = self.generate_random_number(responso.as_str());

                //Si crea un channel locale al client
                //(la parte sender di questo canale deve essere utilizzata dal server per inviare il responso al client).
                let (txc, rxc) = mpsc::channel();

                // Si crea la struttura MsgClient e la si popola con l'id del client che ha inviato il messaggio,
                // con il numero generato dal client e con l'endpoint sender per il server.
                let msg = MsgClient{
                    id_client: self.id_client,
                    msg: generate_num,
                    sender: txc
                };

                println!("Client {} ({:?}): msg inviato -> {}",self.id_client, self.p_type, msg.msg);

                // Si invia il messaggio al server.
                tx.send(msg).unwrap();
                
                // Si riceve il messaggio di risposta del server.
                for received_client in rxc{
                    responso = received_client.clone();
                    println!("Client {} ({:?}): msg ricevuto -> {}", self.id_client, self.p_type,responso);
                }

            }
        }

        /// Metodo che controlla se il client è il vincitore cioè se il mutex contiene ancora 0 o meno.\
        /// Se il mutex contiene 0 vuol dire che il client è il primo che ha indovinato il numero e quindi è il vincitore.\
        /// Se il mutex contiene un numero diverso da 0 vuol dire che un altro client ha indovinato il numero prima.\
        /// Ritorna true se il mutex conteneva 0.\
        /// Ritorna false se il mutex conteneva già un numero.\
        /// 
        /// # Argomenti
        /// 
        /// * 'counter' - Uno SmartPointer Arc che contiene un Mutex che al suo interno contiene un i32
        /// * 'player' - Una struttura Player
        /// 
        /// # Esempi
        /// ```
        /// use std::sync::{Arc,Mutex};
        /// use threads::players::players::Player;
        /// use threads::players::players::PlayerType;
        /// 
        /// let counter = Arc::new(Mutex::new(0));
        /// let mut player = Player{id_client: 1,p_type:PlayerType::Smart,send_number: Some(10),ordine: None,max: 100,min:1};
        /// player.check_win(counter);
        /// ```
        pub fn check_win(self: &mut Player, counter: Arc<Mutex<i32>>)-> bool{
            let mut num_mutex = counter.lock().unwrap();
            if *num_mutex == 0{
                *num_mutex = self.id_client;
                true
            }
            else{
                false
            }
        }
    }

    /// Funzione che genera un client di tipo random tra quelli disponibili (Simple1, Simple2 e Smart).\
    /// Ritorna un'istanza della struttura Player.
    /// 
    /// # Argomenti
    /// 
    /// * 'id_client' - Un i32 che contiene l'id del client
    /// * 'min_range' - Un i32 che contiene il numero minimo del range in cui è compreso il numero pensato dal server
    /// * 'max_range' - Un i32 che contiene il numero massimo del range in cui è compreso il numero pensato dal server
    /// 
    pub fn init_random_player(id_client:i32,min_range:i32, max_range:i32)->Player{

        let id = id_client;
        let mut player_kind = PlayerType::Simple1;
        let mut init_number = None;
        let mut number_order = None;

        let mut rng = rand::thread_rng();
        let player_type = rng.gen_range(0..=2);

        match player_type{
            0=> {
                player_kind = PlayerType::Simple1;
                init_number = Some(0);
                number_order= None;
            }
            1=> {
                player_kind = PlayerType::Simple2;
                init_number = None;
                number_order= None;
            }
            2=> {
                player_kind = PlayerType::Smart;
                init_number = Some(0);
                number_order = None;
            }
            _=> ()
        }
        Player{
            id_client: id,
            p_type:player_kind,
            send_number: init_number,
            ordine: number_order,
            max: max_range,
            min:min_range
        }
    }
}