//! Modulo Server.
//! 
//! Fornisce la funzione per la ricezione dei messaggi.
//! 
/// Modulo del server.
pub mod server{
    
    use std::{sync::mpsc::Receiver};
    use crate::players::players::MsgClient;

    /// Funzione che gestisce la ricezione dei messaggi da parte del server e l'invio della risposta.\
    /// 
    /// # Argomenti
    /// 
    /// * 'rx' - Un endpoint Receiver di un channel 
    /// * 'num_to_guess' - Un i32 che contiene il numero pensato dal server e che i client devono indovinare
    /// 
    pub fn receive_server(rx: Receiver<MsgClient>, num_to_guess:i32){
        
        for received in rx{

            let id_client = received.id_client;
            let msg = received.msg;
            let sender = received.sender;

            println!("Server: msg ricevuto dal client {} -> {}",id_client, msg);

            if msg > num_to_guess{
                //il server deve inviare al client che il numero inviato dal client è maggiore di quello giusto
                sender.send(String::from(">")).unwrap();
            }
            else if msg < num_to_guess{
                //il server deve inviare al client che il numero inviato dal client è minore di quello giusto
                sender.send(String::from("<")).unwrap();
            }
            else{
                //il server deve inviare al client che ha indovinato il numero
                sender.send(String::from("=")).unwrap();
            }
        }


    }
}