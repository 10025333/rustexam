use std::io::stdin;
use std::sync::{Arc,Mutex, mpsc};
use std::{thread};
use std::time::Duration;
use rand::Rng;

pub mod server;
pub mod players;

use crate::players::players::*;
use crate::server::server::*;

fn main() {
  
  let mut input = String::new();

  // Inserimento del numero di giocatori.
  println!("Digitare il numero di giocatori : ");
  stdin().read_line(&mut input).expect("Errore nell'inserimento del valore");
  let mut n_player = input.trim().parse::<i32>().unwrap_or(1);

  input.clear();

  // Inserimento del limite minimo del numero da indovinare.
  println!("Digitare il limite minimo del numero da indovinare : ");
  stdin().read_line(&mut input).expect("Errore nell'inserimento del valore");
  let num_min = input.trim().parse::<i32>().unwrap_or(1);

  input.clear();

  // Inserimento del limite massimo del numero da indovinare.
  println!("Digitare il limite massimo del numero da indovinare : ");
  stdin().read_line(&mut input).expect("Errore nell'inserimento del valore");
  let mut num_max = input.trim().parse::<i32>().unwrap_or(10);

  if num_max <= num_min{
    num_max = num_min * 2;
    println!("E' stato inserito un limite massimo minore del limite minimo.\nIl limite massimo è stato impostato a {}", num_max);
  }
  
  // Creazione del canale per la comunicazione tra il server e i client.
  let (tx, rx) = mpsc::channel();

  // Creazione del canale per la comunicazione tra il server e il main.
  let (tx_server_main, rx_server_main) = mpsc::channel();
  
  // Creazione del thread del server
  thread::spawn(move ||{

      let mut rng = rand::thread_rng();
      let num_to_guess = rng.gen_range(num_min..num_max);
      println!("Numero pensato dal server:{}\n\n", num_to_guess);
      tx_server_main.send("").unwrap();
      receive_server(rx, num_to_guess);
      
  });

  // Ricezione del messaggio del server da parte del main.
  rx_server_main.recv().unwrap();

  thread::sleep(Duration::from_secs(1));

  // Creazione del canale utilizzato dal client che vince per comunicare al main la vittoria.
  let (tx_main, rx_main) = mpsc::channel();

  // Creazione del mutex per permettere al vincitore di comunicare il numero indovinato agli altri client.
  let counter = Arc::new(Mutex::new(0));
  
  let mut id_client = 0;

  // Creazione di un thread per ogni giocatore
  while n_player > 0{

      let tx_client = tx.clone();
      let tx_client_main = tx_main.clone();
      let counter = Arc::clone(&counter);

      thread::spawn(move || {
          
          let mut player = init_random_player(id_client, num_min, num_max);
          player.client(tx_client);

          // Il client corrente controlla di essere il vincitore.
          let is_win = player.check_win(counter);

          // Se il client corrente è il vincitore invia un messaggio al main.
          if is_win == true{
            tx_client_main.send(player.id_client).unwrap();
          }

      });
      n_player -= 1;
      id_client += 1;
  }
  
  // Ricezione sul thread main del fatto che un giocatore ha vinto
  let winner = rx_main.recv().unwrap();

  // Stampa del vincitore
  println!("Vincitore = {:?}", winner);

}
