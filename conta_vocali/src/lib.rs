#[derive(PartialEq,Debug)]
/// Struttura che contiene le vocali.
pub struct NumVocali{ 
    /// Quantità vocali 'a' trovate nella frase.
    a: i32, 
    /// Quantità vocali 'e' trovate nella frase.
    e: i32, 
    /// Quantità vocali 'i' trovate nella frase.
    i: i32, 
    /// Quantità vocali 'o' trovate nella frase.
    o: i32, 
    /// Quantità vocali 'u' trovate nella frase.
    u: i32, 
}

/// Funzione che conta le vocali presenti in una stringa passata in input.\
/// Restituisce una struttura NumVocali contenente per ogni vocale la quantità presente nella stringa.
/// 
/// # Argomenti
/// 
/// * 's' - riferimento alla stringa passata in input
/// 
/// # Esempi
/// ```
/// use doc.NumVocali;
/// let n_voc=String::from("Ciao Paola come stai? Ok. Tu John come stai? Ok");
/// ```
pub fn num_vocali_struct(s:&String) -> NumVocali{
    let caratteri : Vec<_> = s.chars().collect();
    let num_vocali = caratteri.iter().fold(NumVocali{a:0,e:0,i:0,o:0,u:0}, |acc:NumVocali,x|{
        match x{
            'a'|'A'=>NumVocali{a:acc.a+1, ..acc},
            'e'|'E'=>NumVocali{e:acc.e+1, ..acc},
            'i'|'I'=>NumVocali{i:acc.i+1, ..acc},
            'o'|'O'=>NumVocali{o:acc.o+1, ..acc},
            'u'|'U'=>NumVocali{u:acc.u+1, ..acc},
            _=>NumVocali{a:acc.a, e:acc.e, i:acc.i, o:acc.o, u:acc.u},
        }
        
    });
    num_vocali
}

#[derive(PartialEq,Debug)]
/// Tupla di cinque elementi.\
/// Il primo elemento è la quantità di vocali 'a' presenti nella frase.\
/// Il secondo elemento è la quantità di vocali 'e' presenti nella frase.\
/// Il secondo elemento è la quantità di vocali 'i' presenti nella frase.\
/// Il secondo elemento è la quantità di vocali 'o' presenti nella frase.\
/// Il secondo elemento è la quantità di vocali 'u' presenti nella frase.
pub struct TuplaVocali(i32,i32,i32,i32,i32);


/// Funzione che conta le vocali presenti in una stringa passata in input.\
/// Restituisce una tupla TuplaVocali contenente per ogni vocale la quantità presente nella stringa.
/// 
/// # Argomenti
/// 
/// * 's' - Riferimento alla stringa passata in input
/// 
/// # Esempi
/// ```
/// use doc.NumVocali;
/// let t_voc=String::from("Ciao Paola come stai? Ok. Tu John come stai? Ok");
/// ```
pub fn num_vocali_tuple(s:&String) -> TuplaVocali{
    let caratteri : Vec<_> = s.chars().collect ();
    let num_vocali = caratteri.iter().fold(TuplaVocali(0,0,0,0,0), |acc,x| {
        match x {
            'a'|'A' => TuplaVocali{0:acc.0+1, ..acc},
            'e'|'E' => TuplaVocali{1:acc.1+1, ..acc},
            'i'|'I' => TuplaVocali{2:acc.2+1, ..acc},
            'o'|'O' => TuplaVocali{3:acc.3+1, ..acc},
            'u'|'U' => TuplaVocali{4:acc.4+1, ..acc},
             _ => TuplaVocali(acc.0,acc.1, acc.2, acc.3, acc.4),
        }
    });
    num_vocali
}

#[cfg(test)]
mod test{
    use super::*;

    #[test]
    /// Test sulla funzione per contare le vocali utilizzando una struttura e sulla funzione per contare le vocali utilizzando una tupla
    fn test_folds(){
        
        let a=String::from("Ciao Paola come stai? Ok. Tu John come stai? Ok");
        assert_eq!(TuplaVocali(5, 2, 3, 7, 1),num_vocali_tuple(&a));
        assert_eq!(NumVocali{a:5,e:2,i:3,o:7,u:1},num_vocali_struct(&a));
    }

}



