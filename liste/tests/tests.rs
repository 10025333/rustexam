use liste::*;

#[test]
/// Test sul metodo len della struttura LinkedList.
fn test_length() {

    let mut list = LinkedList::new();

    list.push(1);
    assert!(list.len() == 1);

    list.push(2);
    assert!(list.len() == 2);

    list.push_unique(3).unwrap();
    assert!(list.len() == 3);

    println!("{}", list);
    list.pop();
    assert!(list.len() == 2);
}

#[test]
/// Test sul metodo push della LinkedList.
fn test_values() {
    let mut list = LinkedList::new();

    list.push(String::from("string 1"));
    list.push(String::from("string 2"));


    assert_eq!(list.pop(), Some(String::from("string 2")));
    assert_eq!(list.pop(), Some(String::from("string 1")));
}

#[test]
/// Test sui metodi push e pop.
fn test_none() {
    let mut list = LinkedList::new();

    list.push(1);
    list.pop();

    assert_eq!(list.pop(), None);
}


#[test]
/// Test sul metodo push_unique in caso si cerchi d'inserire un numero già esistente
fn test_errors() {
    let mut list = LinkedList::new();

    list.push_unique(2).unwrap();

    match list.push_unique(2) {
        Ok(()) => assert!(false),
        Err(_) => assert!(true)
    }
}

#[test]
/// Test sulla stampa della LinkedList
fn test_print() {
    let mut list = LinkedList::new();
    list.push(100);
    list.push(101);
    list.push(102);
    list.push(103);
    println!("{}", list);

}
