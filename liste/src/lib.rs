use std::fmt::{self, Display,Formatter, Debug};
use std::{mem};
#[derive(PartialEq,Clone)]

/// Struttura che contiene un nodo.
pub struct Node<T> {
    /// Puntatore al nodo successivo.
    pub next: Option<Box<Node<T>>>,
    /// Dato del nodo.
    pub data: T
}

impl<T:Clone + PartialEq> Node<T>{

    /// Metodo ricorsivo che aggiunge un nuovo nodo contenente il dato item dopo l'ultimo nodo.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura Nodo
    /// * 'item' - Un dato di tipo generico T
    /// 
    fn push(&mut self, item:T){
        match &mut self.next{
            // Se il nodo corrente non è l'ultimo, si richiama ricorsivamente il metodo.
            Some(i)=>{
                let nodo_successivo = i;
                nodo_successivo.push(item);
            },
            // Se il nodo corrente è l'ultimo, si aggiunge il nuovo nodo dopo il nodo corrente.
            None=>{
                let element_to_add = Node{
                    next: None,
                    data: item
                };
                let nodo = Some(Box::new(element_to_add));
                self.next = nodo;
            }
        }
    }

    /// Metodo ricorsivo che aggiunge un nuovo nodo contenente un dato dopo l'ultimo nodo se non esiste già.\
    /// Ritorna un Result.\
    /// Err se il dato del nuovo nodo è già presente nella lista.\
    /// Ok se il dato del nuovo nodo non è presente nella lista.\
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura Nodo
    /// * 'item' - Un dato di tipo generico T
    /// 
    fn push_unique(&mut self, item:T)-> Result<(), String>{

        if self.data == item{
            return Err(String::from("Errore. Elemento già presente nella lista"));
        }
        match &mut self.next{
            // Se il nodo corrente non è l'ultimo nodo, si richiama ricorsivamente il metodo.
            Some(i)=>{
                let nodo_successivo = i;
                nodo_successivo.push_unique(item)?;
            },
            // Se il nodo corrente è l'ultimo nodo, si aggiunge il nodo dopo il nodo corrente e si ritorna la variante Ok di Result.
            None=>{
                    let element_to_add = Node{
                        next: None,
                        data: item
                    };
                    let nodo = Some(Box::new(element_to_add));

                    self.next = nodo;
                
            }
        }
        Ok(())
    }

    /// Metodo ricorsivo che elimina l'ultimo nodo della lista e lo restituisce.\
    /// Ritorna un Option.\
    /// Some contenente il dato del nodo eliminato.\
    /// None quando la ricorsione arriva all'ultimo nodo della lista.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura Nodo
    /// 
    fn pop(&mut self)->Option<T>{
        let result;
        let follow = mem::replace(&mut self.next, None);
        match follow{
            // Se il nodo corrente è l'ultimo nodo, ritorna None.
            None=>{
                result = None;
            }
            // Se il nodo corrente non è l'ultimo nodo, 
            // si prende il nodo successivo e si controlla se è l'ultimo richiamando ricorsivamente il metodo pop
            Some(mut node)=>{
                match node.next{
                    None => result = Some(node.data),
                    Some(_) => {
                        result = node.pop();
                        self.next = Some(node);

                    }
                }               
            }
        };
        result
        
    }
}

/// Struttura che contiene un lista linkata.
pub struct LinkedList<T> {
    /// Puntatore al primo nodo della lista
    head: Option<Node<T>>,
    /// Lunghezza della lista
    len: i32
}

impl<T: PartialEq + Clone> LinkedList<T>{

    /// Metodo che crea una lista vuota.\
    /// Ritorna una struttura LinkedList.
    /// 
    /// # Esempi
    /// ```
    /// use liste::LinkedList;
    /// 
    /// let mut list = LinkedList::new();
    /// list.push(10);
    /// ```
    pub fn new() -> LinkedList<T>{

        LinkedList{
            head:None,
            len:0,
        }
    }

    /// Metodo che restituisce la lunghezza della Linked List.\
    /// Ritorna un i32 che è la lunghezza della lista.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura LinkedList
    /// 
    /// # Esempi
    /// ```
    /// use liste::LinkedList;
    /// 
    /// let mut list = LinkedList::new();
    /// list.push(2);
    /// list.len();
    /// ```
    pub fn len(&mut self) -> i32{
        self.len
    }

    /// Metodo che inserisce il primo nodo se la lista è vuota altrimenti richiama il metodo push sul nodo.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura LinkedList
    /// * 'item' - Il dato da inserire nella lista
    /// 
    /// # Esempi
    /// ```
    /// use liste::LinkedList;
    /// 
    /// let mut list = LinkedList::new();
    /// list.push(6);
    /// ```
    pub fn push(&mut self, item: T){
        match &mut self.head{

            Some(n)=> {   

                n.push(item);
                self.len +=1;
            },
            None=> {

                let element_first= Node{
                    next: None,
                    data: item
                };

                self.head = Some(element_first);
                self.len +=1;
                
            }
        }
    }

    /// Metodo che inserisce il primo nodo se la lista è vuota altrimenti richiama il metodo push_unique sul nodo.\
    /// Restituisce un Result.\
    /// Ok se l'elemento che si vuole inserire non è presente nella lista.\
    /// Err con una stringa di descrizione dell'errore se l'elemento che si vuole inserire è già presente nella lista.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura LinkedList
    /// * 'item' - Il dato da inserire nella lista
    /// 
    /// # Esempi
    /// ```
    /// use liste::LinkedList;
    /// 
    /// let mut list = LinkedList::new();
    /// list.push_unique(6);
    /// ```
    pub fn push_unique(&mut self, item: T) -> Result<(), String>{

        let result;

        match &mut self.head{
            Some(n)=> {

                result = n.push_unique(item);
                self.len +=1;
            },
            None=> {

                let element_first= Node{
                    next: None,
                    data: item
                };

                self.head = Some(element_first);
                self.len +=1;
                result = Ok(());
            }
            
        }
        result
    }

    /// Metodo che estrae l'ultimo elemento della lista se la lista è composta da un elemento altrimenti 
    /// richiama il metodo pop del nodo.\
    /// Restituisce un Option.\
    /// Some contenente il dato del nodo eliminato se la lista contiene uno o più nodi.\
    /// None se la lista è vuota.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura LinkedList
    /// 
    /// # Esempi
    /// ```
    /// use liste::LinkedList;
    /// 
    /// let mut list = LinkedList::new();
    /// list.push(6);
    /// list.pop();
    /// ```
    pub fn pop(&mut self) -> Option<T>{

        let result;
        let follow = mem::replace(&mut self.head, None);

        match follow{
            // Se la lista è vuota.
            None=>{
                result = None;
            },
            // Se la lista non è vuota.
            Some(mut node)=>{
                match node.next{
                    // Se la lista contiene un solo nodo.
                    None => result = Some(node.data),
                    // Se la lista contiene più di un nodo, si richiama il metodo pop su Node
                    Some(ref _m) => {
                        result = node.pop();
                        //Si ripristina il self.head
                        self.head = Some(node);
                        self.len -=1;
                    }
                }               
            }
        };
        result
    }


}


/// Funzione ricorsiva che stampa i nodi della lista.\
/// Restituisce un Result.\
/// Ok se la stampa è andata a buon fine.\
/// Err se la stampa non è andata a buon fine.
/// 
/// # Argomenti
/// 
/// * 'nodo' - Una struttura Node
/// * 'f' - Il formatter
/// 
fn stampa_elementi <T: Debug>( nodo : &Node <T>, f: &mut Formatter) -> fmt ::Result{
    let mut result= Ok(());
    write!(f, "{:?}", nodo.data)?;
    match &nodo.next{
        None=>(),
        Some(n)=>{
            write!(f, "->")?;
            result = stampa_elementi(&n, f);

        }
    }
    result

}


/// Implementazione del trait Display per stampare la Linked List utilizzando la macro println!
impl<T:PartialEq + Debug> Display for LinkedList<T>{

    fn fmt(&self, f: &mut Formatter<'_>)->fmt::Result{
        let mut result= Ok(());
        match &self.head{
            Some(n)=> {
                result = stampa_elementi(n, f);
            },
            None=> (),
        }
        result
    }
}





