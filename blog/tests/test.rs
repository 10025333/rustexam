use blog::*;

#[test]
/// Test sulla funzione di aggiunta del testo in un Post
fn test_add_text(){
    let mut post = Post::new();

    post.add_text("Ho fatto una camminata!!!");
    assert_eq!("", post.content());
}

#[test]
/// Test sul recupero del contenuto del Post in caso il Post sia in stato Draft.
fn test_content_draft(){
    let mut post = Post::new();

    post.add_text("Ho fatto una camminata!!!");
    assert_eq!("", post.content());
}

#[test]
/// Test sul recupero del contenuto del Post in caso il Post sia in stato PendingReview.
fn test_content_pending_review(){
    let mut post = Post::new();

    post.add_text("Ho fatto una camminata!!!");
    post.request_review();
    assert_eq!("", post.content());
}

#[test]
/// Test sul recupero del contenuto del Post in caso il Post sia in stato Published.
fn test_content_published(){
    let mut post = Post::new();

    post.add_text("Ho fatto una camminata!!!");
    post.request_review();
    post.approve();
    assert_eq!("Ho fatto una camminata!!!", post.content());
}

#[test]
/// Test sull'approvazione di un Post che non è in PendingReview.
fn test_approve_draft(){
    let mut post = Post::new();

    post.add_text("Ho fatto una camminata!!!");
    post.approve();
    assert_eq!("", post.content());
}

#[test]
/// Test sulla richiesta di una review del Post più volte.
fn test_request_review_pending_review(){
    let mut post = Post::new();

    post.add_text("Ho fatto una camminata!!!");
    post.request_review();
    post.request_review();
    assert_eq!("", post.content());
}

#[test]
/// Test sulla richiesta di review del Post dopo l'approvazione del Post.
fn test_request_review_published(){
    let mut post = Post::new();

    post.add_text("Ho fatto una camminata!!!");
    post.request_review();
    post.approve();
    post.request_review();
    assert_eq!("Ho fatto una camminata!!!", post.content());
}

#[test]
/// Test sull'approvazione multipla del Post in stato PendingReview.
fn test_approve_published(){
    let mut post = Post::new();

    post.add_text("Ho fatto una camminata!!!");
    post.request_review();
    post.approve();
    post.approve();
    assert_eq!("Ho fatto una camminata!!!", post.content());
}