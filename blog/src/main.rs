use blog::Post;

use std::io::stdin;
 
fn main() {
	let mut post = Post::new();
	
	let mut input = String::new();

	println!("Digitare il testo del post.\n");
    stdin().read_line(&mut input).expect("Errore nell'inserimento del valore");
    
	// Aggiunta del testo nel Post.
	post.add_text(&input.trim());

	println!("Il contenuto del Post in stato Draft è: {}\n", post.content());

	input.clear();

	println!("Vuoi richiedere la revisione del contenuto del post?\n- Si\n- No");
	stdin().read_line(&mut input).expect("Errore nell'inserimento del valore");
	if input.trim() == "Si" || input.trim() == "si" || input.trim() == "S"{
		
		// Revisione del testo del Post.
		post.request_review();

		println!("Il contenuto del Post in stato RequestReview è: {}\n", post.content());

		// Approvazione del testo del Post.
		post.approve();
		println!("Il contenuto del Post è stato approvato.\n");

		println!("Il contenuto del Post in stato approvato è: {}\n", post.content());
	}

}
