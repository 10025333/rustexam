/// Stuttura che contiene un Post.
pub struct Post{
    /// Stato del Post
    state: Option<Box<dyn State>>,
    /// Contenuto del Post
    content: String
}

impl Post{

    /// Metodo che crea un nuovo Post.\
    /// Ritorna una struttura Post.
    /// 
    /// # Esempi
    /// ```
    /// use blog::Post;
    /// let post = Post::new();
    /// ```
    pub fn new()->Post{
        Post { 
            state: Some(Box::new(Draft{})),
            content: String::new(),
        }
    }
    
    /// Metodo che aggiunge un testo al contenuto del Post.\
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura Post
    /// * 'text' - Un riferimento a str
    /// 
    /// # Esempi
    /// ```
    /// use blog::Post;
    /// let mut post = Post::new();
    /// post.add_text("Post di prova");
    /// ```
    pub fn add_text(&mut self, text:&str){
        self.content.push_str(text);
    }

    /// Metodo che richiama la funzione di content per recuperare il contenuto del Post in base allo stato del Post.\
    /// Ritorna il contenuto del Post.\
    /// Se il Post è in stato Published ritorna il contenuto altrimenti ritorna la stringa vuota.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura Post
    /// 
    /// # Esempi
    /// ```
    /// use blog::Post;
    /// let mut post = Post::new();
    /// post.add_text("Post di prova");
    /// post.content();
    /// ```
    pub fn content(&self)->&str{
        self.state.as_ref().unwrap().content(self)
    }

    /// Metodo che richiama la funzione di request_review definita per lo stato in cui si trova il Post.\
    /// Se lo stato del Post è Draft, il Post passa allo stato PendingReview, altrimenti lo stato del Post rimane invariato.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura Post
    /// 
    /// # Esempi
    /// ```
    /// use blog::Post;
    /// let mut post = Post::new();
    /// post.add_text("Post di prova");
    /// post.request_review();
    /// ```
    pub fn request_review(&mut self){
        let option_state =  self.state.take();
        match option_state{
            Some(state)=>{
                self.state = Some(state.request_review());
            },
            None => ()
        }
        
    }

    /// Metodo che richiama la funzione di approve definita per lo stato in cui si trova il Post.\
    /// Se lo stato del Post è PendingReview, il Post passa allo stato Published, altrimenti lo stato del Post rimane invariato.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura Post
    /// 
    /// # Esempi
    /// ```
    /// use blog::Post;
    /// let mut post = Post::new();
    /// post.add_text("Post di prova");
    /// post.request_review();
    /// post.approve();
    /// ```
    pub fn approve(&mut self){
        let option_state = self.state.take();
        match option_state{
            Some(state)=>{
                self.state = Some(state.approve());
            }
            None => ()
        }
    }
}


/// Definisce il comportamento nei vari stati che il post può assumere.
trait State{

    /// Dichiarazione del metodo per richiedere una review del Post.\
    /// Ritorna una Box contenente la struttura che definisce lo stato del Post.
    /// 
    fn request_review(self: Box<Self>) -> Box<dyn State>;

    /// Dichiarazione del metodo per approvare il Post.\
    /// Ritorna una Box contenente la struttura che definisce lo stato del Post.
    /// 
    fn approve(self: Box<Self>) -> Box<dyn State>;

    /// Dichiarazione del metodo per ritornare il contenuto del Post.\
    /// Ritorna un letterale di stringa contenente il contenuto del Post.
    /// 
    fn content<'a>(& self, _post:& 'a Post)-> & 'a str{
        ""
    }
}
/// Struttura che definisce lo stato Draft del post.
struct Draft{}

impl State for Draft{

    /// Metodo che crea un Box al cui interno c'è una struct PendingReview.
    /// Ritorna la Box contenente la struttura PendingReview.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Un Box della struttura Draft
    /// 
    fn request_review(self: Box<Self>) -> Box<dyn State>{
        Box::new(PendingReview{})
    }

    /// Metodo che ritorna un Box contenente la struttura Draft.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Un Box della struttura Draft
    /// 
    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }
}

/// Struttura che definisce lo stato PendingReview del post.
struct PendingReview{}

impl State for PendingReview{

    /// Metodo che ritorna un Box contenente la struttura PendingReview.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Un Box della struttura PendingReview
    /// 
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    /// Metodo che crea un Box contenente la struttura Published.
    /// Ritorna la Box contenente la struttura Published.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Un Box della struttura PendingReview
    /// 
    fn approve(self: Box<Self>) -> Box<dyn State> {
        Box::new(Published{})
    }
}

/// Struttura che definisce lo stato Published del post.
struct Published{}

impl State for Published{

    /// Metodo che ritorna un Box contenente la struttura Published.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Un Box della struttura Published
    /// 
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    /// Metodo che ritorna un Box contenente la struttura Published.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Un Box della struttura Published
    /// 
    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }

    /// Metodo che ritorna un riferimento al campo content della struttura Post passata in input.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Un riferimento alla struttura Published
    /// * 'post' - Una struttura Post
    /// 
    fn content<'a>(& self, post:& 'a Post)-> & 'a str {
        &post.content
    }
}