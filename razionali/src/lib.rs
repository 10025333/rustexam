use std::ops::Add;
use std::ops::Mul;

#[derive(Debug,PartialEq,Clone, Copy)]
/// Struttura che contiene un numero razionale
pub struct Rational{
    /// Numeratore del numero razionale.
    pub num: i32,
    /// Denominatore del numero razionale.
    pub denom: i32,
}

impl Rational{
    /// Metodo che effettua la somma tra due numeri razionali.\
    /// Ritorna una struttura Rational contenente il numero razionale che è la somma dei due parametri passati in input.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura Rational che rappresenta un numero razionale.
    /// * 'raz_to_add' - Una struttura Rational che rappresenta un numero razionale.
    /// 
    /// # Esempi
    /// ```
    /// use razionali::Rational;
    /// 
    /// let num_raz = Rational{num:12,denom:15};
    /// let result = num_raz.somma(Rational{num:3,denom:10});
    /// ```
    pub fn somma(self, raz_to_add: Rational)->Rational{
        
        let numeratore;
        let denominatore;
        let mut mcm = 1;

        if self.denom == raz_to_add.denom{
            numeratore = self.num + raz_to_add.num;
            denominatore = self.denom;
        }
        else{
            //Calcolo dell'minimo comune multiplo tra i denominatori dei due numeri razionali
            if self.denom < raz_to_add.denom{
                mcm = raz_to_add.denom;
            }
            else if self.denom > raz_to_add.denom{
                mcm = self.denom;
            }
            while mcm %self.denom!= 0 || mcm%raz_to_add.denom !=0 {
                mcm +=1;
            }

            numeratore = (self.num*(mcm/self.denom)) + (raz_to_add.num*(mcm/raz_to_add.denom));
            denominatore = mcm;
        }
        
        Rational{
            num: numeratore,
            denom: denominatore,
        }
    }

    /// Metodo che effettua il prodotto tra due numeri razionali.\
    /// Ritorna una struttura Rational contenente un numero razionale che è il prodotto dei due numeri razionali passati in input.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura Rational che rappresenta un numero razionale
    /// * 'raz_to_multiply' - Una struttura Rational che rappresenta un numero razionale
    /// 
    /// # Esempi
    /// ```
    /// use razionali::Rational;
    /// 
    /// let num_raz = Rational{num:12,denom:5};
    /// let result = num_raz.prodotto(Rational{num:3,denom:10});
    /// ```
    pub fn prodotto(self, raz_to_multiply: Rational)-> Rational{

        let numeratore = self.num * raz_to_multiply.num;
        let denominatore = self.denom * raz_to_multiply.denom;
        
        Rational{
            num: numeratore,
            denom: denominatore,
        }
    }

    /// Metodo che effettua la riduzione ai minimi termini di un numero razionale.\
    /// Ritorna una struttura Rational contenente un numero razionale ridotto ai minimi termini.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura Rational che rappresenta un numero razionale
    /// 
    /// # Esempi
    /// ```
    /// use razionali::Rational;
    /// 
    /// let num_raz = Rational{num:3,denom:12};
    /// let result = num_raz.riduci();
    /// ```
    pub fn riduci(self)->Rational{

        let mut mcd = 1;
        let mut cont = 1;

        //Calcolo dell'MCD
        while cont <= self.num && cont <= self.denom{

            if self.num%cont == 0 && self.denom%cont == 0{
                mcd = cont;
            }
            cont = cont + 1;
        }

        let numeratore = self.num/mcd;
        let denominatore = self.denom/mcd;

        Rational { 
            num: numeratore, 
            denom: denominatore,
        }
    }

    /// Metodo che trasforma un numero intero in numero razionale.\
    /// Ritorna una struttura Rational con numeratore uguale al parametro in input e come denominatore 1.
    /// 
    /// # Argomenti
    /// 
    /// * 'number' - Un numero intero i32
    /// 
    /// # Esempi
    /// ```
    /// use razionali::Rational;
    /// 
    /// let result = Rational::base(5);
    /// ```
    pub fn base(number : i32)->Rational{
        Rational { num: number, denom: 1 }
    }

}

/// Implementazione del trait Add per la struttura dei numeri razionali.
impl Add for Rational{
    type Output = Self;
    fn add(self, other:Rational)->Self::Output{
        self.somma(other)
    }
}

///Implementazione del trait Mul per la struttura dei numeri razionali
impl Mul for Rational{
    type Output = Self;
    fn mul(self, other:Rational)->Self::Output{
        self.prodotto(other)
    }
}
