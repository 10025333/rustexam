use razionali::{Rational};
#[test]
// Test sulla somma tra numeri razionali con denominatore uguale o denominatore diverso. 
// Richiama il metodo somma sul razionale raz1 passando in input la variabile raz2 anch'essa razionale.
// La prima volta che è richiamato il metodo somma i due numeri razionali hanno lo stesso denominatore.
// La seconda volta che è richiamato il metodo somma i due numeri razionali hanno denominatore diverso.
pub fn test_somma(){
    let mut raz1:Rational;
    let mut raz2:Rational;
    let mut result:Rational;

    //Somma numeri razionali con denominatore uguale
    raz1 = Rational{
        num: 10,
        denom: 6,
    };
    raz2 = Rational{
        num: 20,
        denom:6,
    };
    result = Rational{
        num: 30,
        denom: 6,
    };
    assert_eq!(raz1.somma(raz2), result);

    //Somma numeri razionali con denominatore uguale
    raz1 = Rational{
        num: -2,
        denom: 3,
    };
    raz2 = Rational{
        num: 7,
        denom:9,
    };
    result = Rational{
        num: 1,
        denom: 9,
    };
    assert_eq!(raz1.somma(raz2), result);
}

#[test]
/// Test sul prodotto tra numeri razionali.
/// Richiama il metodo prodotto sul razionale raz1 passando in input la variabile raz2 anch'essa razionale.
pub fn test_prodotto(){
    let raz1 = Rational{
        num: 5,
        denom: 3,
    };
    let raz2 = Rational{
        num: 2,
        denom:10,
    };
    let result = Rational{
        num: 10,
        denom: 30,
    };
    assert_eq!(raz1.prodotto(raz2), result);
}
#[test]
/// Test sulla riduzione ai minimi termini di un numero razionale.
/// Richiama il metodo riduci sul razionale raz.
pub fn test_riduci(){
    let raz = Rational{
        num: 18,
        denom: 84,
    };
    let result = Rational{
        num: 3,
        denom: 14,
    };
    assert_eq!(raz.riduci(), result);
}

#[test]
/// Test sulla creazione di un numero razionale con denominatore 1 partendo da un i32.
/// Richiama la funzione base passando in input la variabile number.
pub fn test_base(){
    let number = 5;
    let result = Rational{
        num: 5,
        denom: 1,
    };
    assert_eq!(Rational::base(number), result);
}
#[test]
/// Test sull'implementazione dei trait Add e Mul per la struttura che gestisce i numeri razionali.
/// Utilizza l'operatore + per sommare due numeri razionali.
/// Utilizza l'operatore * per moltiplicare due numeri razionali.
pub fn test_impl(){
    let r1 = Rational{
        num: 5,
        denom: 2
    };
    let r2= Rational{
        num: 6,
        denom: 5
    };
    let r_add= Rational{
        num:37,
        denom:10
    };
    let r_mul= Rational{
        num:30,
        denom:10
    };
    assert_eq!(r1+r2, r_add);
    assert_eq!(r1*r2, r_mul);

}
