use gioco::game::lib_configurazione::*;
use gioco::game::lib_init::*;
use gioco::game::lib_giocatore::*;

/// Test sull'inizializzazione della configurazione di gioco, del giocatore e stampa del campo di gioco.
#[test]
fn test_init_configurazione(){

    let conf: Configurazione = init_configurazione(4, 4, 5, 5, 20, 40);
    let giocatore = Giocatore::new(TipoDirezione::Nessuna,1,10);

    stampa_campo_gioco(&conf, &giocatore);
}

/// Test sull'inizializzazione del giocatore.
#[test]
fn test_init_giocatore(){
    let giocatore = Giocatore{direzione:TipoDirezione::Nessuna, posizione:1, forza:1  };
    assert_eq!(giocatore, Giocatore::new(TipoDirezione::Nessuna,1,1));
}

/// Test sul movimento del giocatore quando il lancio della moneta ha esito testa.
#[test]
fn test_move_giocatore_no_random(){

    let conf: Configurazione = init_configurazione(4, 4, 5, 5, 20, 40);
    let mut giocatore1 = Giocatore{direzione:TipoDirezione::Sx, posizione:6, forza:1  };
    let giocatore2 = Giocatore{direzione:TipoDirezione::Nessuna, posizione:5, forza:1  };
    giocatore1.move_giocatore(&conf, false);
    assert_eq!(giocatore2.posizione, giocatore1.posizione );
}

/// Test sul movimento del giocatore nel caso si trovi in una cella con il muro a destra e voglia andare di nuovo a destra.
#[test]
fn test_move_giocatore_celle_con_muro(){

    let conf: Configurazione = init_configurazione(4, 4, 5, 5, 20, 40);
    let mut giocatore1 = Giocatore{direzione:TipoDirezione::Dx, posizione:15, forza:1  };
    let giocatore2 = Giocatore{direzione:TipoDirezione::Nessuna, posizione:14, forza:1  };
    giocatore1.move_giocatore(&conf, false);
    assert_eq!(giocatore2.posizione, giocatore1.posizione );
}


