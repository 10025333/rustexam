use std::io::stdin;
use rand::Rng; 
use std::time::Duration;
use std::{thread};

pub mod game;

use crate::game::lib_init::*;
use crate::game::lib_configurazione::*;
use crate::game::lib_giocatore::*;

 
fn main() {
    
    let n;
    let num_celle_non_vuote;
    let forza;
    let max_num_mosse;

    let mut input = String::new();

    // Inserimento del numero di colonne e righe della matrice di gioco.
    println!("Digitare il numero di righe e colonne della matrice di gioco: ");
    stdin().read_line(&mut input).expect("Errore nell'inserimento del valore");
    n = input.trim().parse::<i32>().unwrap_or(1);

    input.clear();

    
    // Definizione del numero di celle che conterranno cibo e del numero di celle che conterranno veleno.
    num_celle_non_vuote = n +1;

    // Inserimento del valore di partenza della forza del giocatore.
    println!("Digitare con quanta forza parte il giocatore: ");
    stdin().read_line(&mut input).expect("Errore nell'inserimento del valore");
    forza = input.trim().parse::<i32>().unwrap_or(1);

    input.clear();

    // Inserimento del numero massimo di mosse che può fare il giocatore.
    println!("Digitare il numero massimo di mosse che può effettuare il giocatore: ");
    stdin().read_line(&mut input).expect("Errore nell'inserimento del valore");
    max_num_mosse = input.trim().parse::<i32>().unwrap_or(1);

    let mut moneta;
    let mut i = 1;

    // Inizializzazione della configurazione di gioco.
    let mut conf: Configurazione = init_configurazione(n,
                                                        n,
                                                        num_celle_non_vuote, 
                                                        num_celle_non_vuote, 
                                                        max_num_mosse,
                                                        forza);

    // Inizializzazione del giocatore.
    let mut giocatore = Giocatore::new(TipoDirezione::Nessuna, 
                                                    conf.pos_giocatore,
                                                    forza);
    println!("Campo di gioco:\n{}", conf);
    println!("Giocatore:\n{}", giocatore);

    stampa_campo_gioco(&conf,&giocatore);

    thread::sleep(Duration::from_secs(3));
    
    while conf.numero_max_mosse > 0 && giocatore.forza > 0{

        println!("----------------------------");
        println!("Mossa {}\n", i);

        //Lancio moneta
        moneta = rand::thread_rng().gen_range(0..=1);
        if moneta == 0{
            println!("Moneta: Croce\n");
        }
        else{
            println!("Moneta: Testa\n");
        }

        //Se testa e non si è alla prima mossa si muove nella direzione scelta
        if moneta == 1 && giocatore.direzione != TipoDirezione::Nessuna{
            
            giocatore.move_giocatore(&conf, false);
        }
        else{
            //Se croce si muove in una direzione a caso
            giocatore.move_giocatore(&conf, true);
        }

        let modifica_forza = giocatore.aggiornamento_forza(&conf);

        if modifica_forza > 0{
            println!("Il giocatore aumenta la forza: +{}\n", modifica_forza);
        }
        else if modifica_forza < 0{
            println!("Il giocatore diminuisce la forza: {}\n", modifica_forza);
        }

        // Aggiornamento della posizione del giocatore nella configurazione.
        conf.pos_giocatore = giocatore.posizione;

        // Aggiornamento del numero massimo di mosse.
        conf.numero_max_mosse -= 1;

        println!("Giocatore:\n{}", giocatore);

        stampa_campo_gioco(&conf,&giocatore);
        
        thread::sleep(Duration::from_secs(2));

        i+=1;
    }

    if giocatore.forza <= 0{
        println!("Hai perso!!!");
    }

    if giocatore.forza > 0{
        println!("Hai vinto!!!");
    }
}

