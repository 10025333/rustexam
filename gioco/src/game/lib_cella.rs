use std::fmt::{Formatter, Result, Display};

/// Struttura che definisce il tipo di cella
pub struct TipoCella{
    /// Se true, cella senza muri
    pub normale: bool,
    /// Se true, cella con muro sopra
    pub muro_sopra: bool,
    /// Se true, cella con muro sotto
    pub muro_sotto: bool,
    /// Se true, cella con muro a destra
    pub muro_dx: bool,
    /// Se true, cella con muro a sinistra
    pub muro_sx: bool
}

#[derive(PartialEq, Clone)]
/// Enumeratore che definisce il contenuto della cella
pub enum ContenutoCella{
    Vuoto,
    Cibo,
    Veleno,
}

/// Implementazione del trait Display per la struttura ContenutoCella
impl Display for ContenutoCella {
    fn fmt(&self, f:&mut Formatter)-> Result{
        let stringa_contenuto;

        match self{
            ContenutoCella::Vuoto => stringa_contenuto = " ",
            ContenutoCella::Cibo => stringa_contenuto = "C",
            ContenutoCella::Veleno => stringa_contenuto = "V",
        };
        write!(f, "{}", stringa_contenuto)
    }
}

/// Struttura che contiene una cella.
pub struct Cella{
    /// Tipo della cella
    pub tipo: TipoCella,
    /// Contenuto della cella
    pub contenuto: ContenutoCella,
    /// Valore positivo o negativo della cella (vale solo se il contenuto è diverso da Vuoto)
    pub valore: i32,
}


