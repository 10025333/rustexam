use super::{lib_cella::*};

use std::fmt::{Formatter, Result, Display};

/// Struttura che contiene la configurazione di gioco
pub struct Configurazione{
    /// Campo di gioco
    pub campo_gioco: Vec<Cella>,
    /// Posizione del giocatore nel campo di gioco
    pub pos_giocatore: i32,
    /// Numero di righe del campo di gioco
    pub numero_righe: i32,
    /// Numero di colonne del campo di gioco
    pub numero_colonne:i32,
    /// Numero massimo di mosse consentite al giocatore
    pub numero_max_mosse: i32
}

/// Implementazione del trait Display per la struttura Configurazione
impl Display for Configurazione{

    fn fmt(&self, f: &mut Formatter<'_>)->Result{
        
        write!(f, "- Numero righe:{}\n- Numero colonne:{}\n- Numero max mosse:{}\n",
                self.numero_righe,
                self.numero_colonne,
                self.numero_max_mosse)?;
        Ok(())
        
    }
}
