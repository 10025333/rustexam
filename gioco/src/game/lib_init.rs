use super::lib_giocatore::*;
use super::lib_cella::*;
use super::lib_configurazione::*;

use rand::Rng;  

/// Funzione che inizializza la configurazione del gioco.\
/// Ritorna una struttura Configurazione.
/// 
/// # Argomenti
/// 
/// * 'num_righe' - Un i32 che definisce il numero di righe del campo di gioco
/// * 'num_colonne' - Un i32 che definisce il numero di colonne del campo di gioco
/// * 'num_cibo' - Un i32 che definisce il numero di celle che conterranno cibo
/// * 'num_veleno' - Un i32 che definisce il numero di celle che conterranno veleno
/// * 'num_max_mosse' - Un i32 che definisce il numero massimo di mosse del giocatore
/// * 'forza' - Un i32 che definisce la forza del giocatore
/// 
/// # Esempi
/// ```
/// use gioco::game::lib_configurazione::Configurazione;
/// use gioco::game::lib_init::init_configurazione;
/// 
/// let conf: Configurazione = init_configurazione(4, 4, 5, 5, 20, 40);
/// ```
pub fn init_configurazione(num_righe:i32, 
                            num_colonne:i32,
                            num_cibo:i32, 
                            num_veleno:i32, 
                            num_max_mosse: i32, 
                            forza:i32)->Configurazione{
    
    let numero_celle = num_righe * num_colonne;
    // Inizializzazione del campo di gioco.
    let campo = init_campo(num_righe, num_colonne, num_cibo, num_veleno, forza);
    
    //Posizione iniziale random del giocatore.
    let mut posizione_giocatore = rand::thread_rng().gen_range(0..numero_celle);
    while campo[posizione_giocatore as usize].contenuto != ContenutoCella::Vuoto {
        posizione_giocatore = rand::thread_rng().gen_range(0..numero_celle);
    }
    
    Configurazione{
        campo_gioco:campo,
        pos_giocatore: posizione_giocatore,
        numero_colonne: num_colonne,
        numero_righe: num_righe,
        numero_max_mosse: num_max_mosse
    }
}

/// Funzione che definisce in modo randomico quali celle conterranno cibo e quali veleno.
/// 
/// # Argomenti
/// 
/// * 'campo' - Un vettore di strutture Cella che definisce il campo di gioco
/// * 'quantita' - Un i32 che definisce la quantità di celle con cibo o con veleno
/// * 'cont' - Un enum ContenutoCella che definisce se si stanno determinando le celle con cibo o veleno
/// * 'forza' - Un i32 che definisce la forza del giocatore
/// 
fn init_pos(campo: &mut Vec<Cella>,quantita:i32, cont: ContenutoCella, forza:i32){

    let mut p = 0;
    let mut num_rand;
    let mut val_rand;

    while p<quantita {
        
        num_rand = rand::thread_rng().gen_range(0..campo.len());

        if campo[num_rand as usize].contenuto == ContenutoCella::Vuoto {

            // Aggiornamento del contenuto della cella.
            campo[num_rand as usize].contenuto = cont.clone();

            // Aggiornamento del valore della cella.
            val_rand = rand::thread_rng().gen_range(1..forza);
            campo[num_rand as usize].valore = val_rand;

            p +=1;
        }

    }
}

/// Funzione che inizializza il campo di gioco.\
/// Ritorna un vettore di strutture Cella.
/// 
/// # Argomenti
/// 
/// * 'num_righe' - Un i32 che definisce il numero di righe del campo di gioco
/// * 'num_colonne' - Un i32 che definisce il numero di colonne del campo di gioco
/// * 'quant_cibo' - Un i32 che definisce il numero di celle che conterranno cibo
/// * 'quant_veleno' - Un i32 che definisce il numero di celle che conterranno veleno
/// * 'forza' - Un i32 che definisce la forza del giocatore
/// 
fn init_campo(numero_righe:i32, numero_colonne:i32, quant_cibo:i32, quant_veleno:i32, forza:i32)-> Vec<Cella>{

    let mut campo:Vec<Cella> = Vec::new();
    let mut i = 0;
    let numero_celle = numero_colonne*numero_righe;
    let mut cella;
    let mut tipo_cella;
    let contenuto_cella = ContenutoCella::Vuoto;
    
    
    while i < numero_celle{

        //Prima riga
        if i<numero_colonne {
            // Prima cella della prima riga.
            if i%numero_colonne==0 {

                tipo_cella = TipoCella{
                    normale: false,
                    muro_sopra: true,
                    muro_sotto: false,
                    muro_dx: false,
                    muro_sx: true
                };
            }
            // Ultima cella della prima riga.
            else if i%numero_colonne==numero_colonne-1{
                tipo_cella = TipoCella{
                    normale: false,
                    muro_sopra: true,
                    muro_sotto: false,
                    muro_dx: true,
                    muro_sx: false
                };
            }
            // Tutte le altre celle della prima riga.
            else{
                tipo_cella = TipoCella{
                    normale: false,
                    muro_sopra: true,
                    muro_sotto: false,
                    muro_dx: false,
                    muro_sx: false
                };
            }
            
        }//Ultima riga
        else if i>(numero_righe*numero_colonne - numero_colonne){

            // Prima cella dell'ultima riga.
            if i%numero_colonne==0 {
                tipo_cella = TipoCella{
                    normale: false,
                    muro_sopra: false,
                    muro_sotto: true,
                    muro_dx: false,
                    muro_sx: true
                };
            }
            // Ultima cella dell'ultima riga.
            else if i%numero_colonne==numero_colonne-1{
                tipo_cella = TipoCella{
                    normale: false,
                    muro_sopra: false,
                    muro_sotto: true,
                    muro_dx: true,
                    muro_sx: false
                };
            }
            // Tutte le altre celle dell'ultima riga.
            else{
                tipo_cella = TipoCella{
                    normale: false,
                    muro_sopra: false,
                    muro_sotto: true,
                    muro_dx: false,
                    muro_sx: false
                };
            }
        }
        //Prima cella delle righe in mezzo al campo di gioco.
        else if i%numero_colonne==0{

            tipo_cella = TipoCella{
                normale: false,
                muro_sopra: false,
                muro_sotto: false,
                muro_dx: false,
                muro_sx: true
            };

        }//Ultima cella delle righe in mezzo al campo di gioco.
        else if i%numero_colonne==numero_colonne-1  {

            tipo_cella = TipoCella{
                normale: false,
                muro_sopra: false,
                muro_sotto: false,
                muro_dx: true,
                muro_sx: false
            };

        }
        // Tutte le altre celle.
        else{
            tipo_cella = TipoCella{
                normale: true,
                muro_sopra: false,
                muro_sotto: false,
                muro_dx: false,
                muro_sx: false
            };
        }

        cella = Cella{
            tipo:tipo_cella,
            contenuto:contenuto_cella.clone(),
            valore: 0,
        };

        campo.push(cella);
        i+=1;
    }
    init_pos(&mut campo,quant_cibo, ContenutoCella::Cibo, forza);
    init_pos(&mut campo, quant_veleno, ContenutoCella::Veleno, forza);
    
    campo
}

/// Funzione che stampa il campo di gioco.
/// 
/// # Argomenti
/// 
/// * 'conf' - Una struttura Configurazione
/// * 'player' - Una struttura Giocatore
/// 
pub fn stampa_campo_gioco(conf:&Configurazione, player:&Giocatore){
    let mut numero_cella = 0;
    let mut j = 0;

    // Stampa del muro sopra alla prima riga.
    while j < conf.numero_colonne{
        print!("______");
        j +=1;
    }
    print!("_\n");

    for i in &conf.campo_gioco {

        // Stampa del muro a sinistra della prima cella della riga
        if numero_cella % conf.numero_colonne == 0{
            print!("|");
        } 

        print!( "{}", format!("{:<4}",i.contenuto));

        // Stampa della posizione del giocatore nel campo di gioco
        if player.posizione == numero_cella{
            print!("{}",format!("{:^4}", "X"));
        }
        else{
            print!("{}",format!("{:4}"," "));
        }

        // Stampa del divisore tra le celle
        print!( "{}", "|");

        // Se si è all'ultima cella della riga, si va a capo, 
        // si stampa un divisore tra le celle sopra e sotto e si va di nuovo a capo
        if numero_cella % conf.numero_colonne == conf.numero_colonne-1{
            print!("\n|");
            j=0;
            while j < conf.numero_colonne{
                print!("_____|");
                j +=1;
            }
            print!("\n");
        }
        numero_cella += 1;
    }
}

