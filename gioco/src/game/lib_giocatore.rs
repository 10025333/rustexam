use super::lib_configurazione::*;
use super::lib_cella::*;

use std::fmt::{Formatter, Result, Display};
use rand::Rng;  

#[derive(PartialEq, Debug)]
/// Enumeratore che definisce la direzione dello spostamento del giocatore.
pub enum TipoDirezione{
    Nessuna,
    Su,
    Giu,
    Dx,
    Sx
}
#[derive(PartialEq, Debug)]
/// Struttura che contiene il Giocatore.
pub struct Giocatore{
    /// Direzione di spostamento del giocatore
    pub direzione: TipoDirezione,
    /// Posizione del giocatore nel campo di gioco
    pub posizione: i32,
    /// Forza rimanente del giocatore
    pub forza: i32
}

/// Implementazione del trait Display per la struttura Giocatore
impl Display for Giocatore{
    fn fmt(&self, f:&mut Formatter)-> Result{

        let stringa_dir;

        match self.direzione{
            TipoDirezione::Nessuna => stringa_dir = "Nessuna",
            TipoDirezione::Su => stringa_dir = "Su",
            TipoDirezione::Giu => stringa_dir = "Giu",
            TipoDirezione::Dx => stringa_dir = "Dx",
            TipoDirezione::Sx => stringa_dir = "Sx",
        };
        write!(f, "- Direzione-> {}\n- Posizione-> {}\n- Forza-> {}\n", stringa_dir, self.posizione, self.forza)
    }
}

impl Giocatore{

    /// Funzione che inizializza la struttura del giocatore.\
    /// Ritorna una struttura Giocatore.
    /// 
    /// # Argomenti
    /// 
    /// * 'direzione' - Un enum TipoDirezione
    /// * 'posizione' - Un i32 che contiene l'indice della cella in cui si trova il giocatore.
    /// * 'forza' - Un i32 che definisce la forza del giocatore
    /// 
    /// # Esempi
    /// ```
    /// use gioco::game::lib_giocatore::Giocatore;
    /// use gioco::game::lib_giocatore::TipoDirezione;
    /// 
    /// let giocatore = Giocatore::new(TipoDirezione::Nessuna,1,10);
    /// ```
    pub fn new(direzione: TipoDirezione, posizione:i32, forza:i32)->Giocatore{
        Giocatore{
            direzione: direzione,
            posizione:posizione,
            forza:forza,
        }
    }

    /// Metodo che setta la direzione in cui il giocatore si deve muovere.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura Giocatore
    /// * 'dir' - Un i32 che definisce la direzione in cui il giocatore deve muoversi
    /// 
    fn set_direzione(self: &mut Giocatore, dir: i32){
        if dir == 0{
            self.direzione = TipoDirezione ::Su;
        }
        else if dir == 1{
            self.direzione = TipoDirezione::Giu;
        }
        else if dir == 2{
            self.direzione = TipoDirezione::Dx;
        }
        else{
            self.direzione = TipoDirezione::Sx;
        }
    }

    /// Metodo che aggiorna la posizione del giocatore in base alla direzione scelta.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura Giocatore
    /// * 'conf' - Una struttura Configurazione
    /// * 'is_random' - Un bool che se vale true genera una direzione random altrimenti no
    /// 
    /// # Esempi
    /// ```
    /// use gioco::game::lib_giocatore::Giocatore;
    /// use gioco::game::lib_giocatore::TipoDirezione;
    /// use gioco::game::lib_configurazione::Configurazione;
    /// use gioco::game::lib_init::init_configurazione;
    /// 
    /// let conf: Configurazione = init_configurazione(4,4,5, 5, 20,40);
    /// let mut giocatore = Giocatore::new(TipoDirezione::Nessuna,1,10);
    /// giocatore.move_giocatore(&conf, false);
    /// ```
    pub fn move_giocatore(self: &mut Giocatore,conf:& Configurazione,  is_random:bool){
        if is_random == true{
            let dir = rand::thread_rng().gen_range(0..4);
            self.set_direzione(dir);
            
        }
        if self.direzione == TipoDirezione::Su{

            // Se il giocatore è dalla seconda riga in giù, si sposta in su altrimenti nella direzione opposta.
            if self.posizione >= conf.numero_colonne{
                self.posizione = self.posizione - conf.numero_colonne;
            }
            else{
                self.posizione = self.posizione + conf.numero_colonne;
            }
        }
        else if self.direzione == TipoDirezione::Giu{

            // Se il giocatore è dalla penultima riga in sù, si sposta in giù altrimenti nella direzione opposta.
            if self.posizione <= (conf.numero_colonne * conf.numero_righe) - conf.numero_colonne {
                self.posizione = self.posizione + conf.numero_colonne;
            }
            else{
                self.posizione = self.posizione - conf.numero_colonne;
            }
        }
        else if self.direzione == TipoDirezione::Dx{

            // Se il giocatore è dalla prima alla penultima cella della riga, si sposta a destra altrimenti nella direzione opposta.
            if self.posizione % conf.numero_colonne < conf.numero_colonne - 1 {
                self.posizione = self.posizione + 1;
            }
            else{
                self.posizione = self.posizione - 1;
            }
        }
        else if self.direzione == TipoDirezione::Sx{

            // Se il giocatore è dalla seconda all'ultima cella della riga, si sposta a sinistra altrimenti nella direzione opposta.
            if self.posizione % conf.numero_colonne >=  1 {
                self.posizione = self.posizione - 1;
            }
            else{
                self.posizione = self.posizione + 1;
            }
        }
    }
    
    /// Metodo che aggiorna il valore della forza del giocatore in base al contenuto della cella in cui si trova il giorcatore.\
    /// Ritorna un i32 che contiene quanto è stata modificata la forza del giocatore.
    /// 
    /// # Argomenti
    /// 
    /// * 'self' - Una struttura Giocatore
    /// * 'conf' - Una struttura Configurazione
    /// 
    /// # Esempi
    /// ```
    /// use gioco::game::lib_giocatore::Giocatore;
    /// use gioco::game::lib_giocatore::TipoDirezione;
    /// use gioco::game::lib_configurazione::Configurazione;
    /// use gioco::game::lib_init::init_configurazione;
    /// 
    /// let conf: Configurazione = init_configurazione(4,4,5, 5, 20,40);
    /// let mut giocatore = Giocatore::new(TipoDirezione::Nessuna,1,10);
    /// let modifica_forza = giocatore.aggiornamento_forza(&conf);
    /// ```
    pub fn aggiornamento_forza(self: &mut Giocatore, conf:& Configurazione)-> i32{

        if conf.campo_gioco[self.posizione as usize].contenuto == ContenutoCella::Veleno{
            self.forza -= conf.campo_gioco[self.posizione as usize].valore;
            -conf.campo_gioco[self.posizione as usize].valore
        }
        else if conf.campo_gioco[self.posizione as usize].contenuto == ContenutoCella::Cibo{
            self.forza += conf.campo_gioco[self.posizione as usize].valore;
            conf.campo_gioco[self.posizione as usize].valore
        }
        else{
            0
        }
        
    }
}

